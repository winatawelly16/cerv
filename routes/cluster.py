import os
import requests
import re ## regex

from pathlib import Path  
from flask import Flask, Blueprint,  jsonify, request
from dotenv import load_dotenv

import pandas as pd   
import numpy as np
import pickle as pk

from sklearn.cluster import KMeans

cluster = Blueprint('cluster', __name__, url_prefix='/cluster')

load_dotenv(dotenv_path='./env/staging.env')




headers = {"Authorization": "Bearer "+ str(os.getenv("TOKEN"))}
url = str(os.getenv("URL"))

questionList = [
    'Height?',
    'Weight?',
    'Top Half',
    'Bottom Half',
    'Looks that best represents you everyday',
    "Are there any body areas you'd like to cover up?",
    'What style set are you most comfortable in?',
    'What colour palettes are you most comfortable in?'
]
COVER_TOP = ['Tummy', 'Cleavage', 'Arms', 'Back', 'Shoulders']
COVER_BOTTOM = ['Buttocks', 'Legs']

# =================================================================================================
# FUNCTION
# =================================================================================================


def getCategory(height, weight):
    if( height < 155 and weight < 50 ):
        return 'petite'
    elif( (height >= 155 and height <=168) and (weight >=45 and weight <= 60)):
        return 'regular'
    elif( height > 155 and weight > 65 ):
        return 'plus'
    elif( height > 168 and weight > 60 ):
        return 'tall'
    else:
        if( weight < 50):
            return 'petite'
        elif( weight >=45 and weight <= 60):
            return 'regular'
        elif( weight > 65 ):
            return 'plus'
        else:
            return 'tall'

def createDataFrame(features):
    df = pd.DataFrame(features, index=[0])
    df.rename(columns = {
    'Height?': 'height',
    'Weight?': 'weight',
    'Looks that best represents you everyday': 'looks_represent',
    'What style set are you most comfortable in?': 'style_set',
    'What colour palettes are you most comfortable in?': 'colour_palettes',
    'Top Half': 'top_fit',
    'Bottom Half': 'bot_fit',
    }, inplace=True)
    return df

def getFeatures(data):
    features = {}

    features['cover-top'] = False
    features['cover-bottom'] = False

    
    
    for i in data:
        if i['question'] in questionList:
            if i['question'] == "Are there any body areas you'd like to cover up?":
                if i['answer'] == 'I wear hijab':
                    features['hijab'] = True

                    features['cover-top'] = True
                    features['cover-bottom'] = True
                else:
                    features['hijab'] = False

                    cover = i['answer']
                    if(',' in cover):
                        for cov in cover.split(','):
                            string = cov.strip()
                            if(string in COVER_TOP):
                                features['cover-top'] = 1
                            if(string in COVER_BOTTOM):
                                features['cover-bottom'] = 1
                    else:
                        if(cover in COVER_TOP):
                            features['cover-top'] = 1
                        if(cover in COVER_BOTTOM):
                            features['cover-bottom'] = 1
            else:
                features[i['question']] = i['answer']
    return features

def preprocessing(dataframe):
    data = dataframe.copy(deep=True)

    dislike = []

    for i in range(len(data)):
        data.at[i, 'palette-bright'] = 0
        data.at[i, 'palette-cool'] = 0
        data.at[i, 'palette-warm'] = 0
        data.at[i, 'palette-neutral'] = 0
        data.at[i, 'palette-understated'] = 0
        
        data.at[i, 'topfit-fitted'] = 0
        data.at[i, 'topfit-loose'] = 0
        data.at[i, 'topfit-oversized'] = 0
        data.at[i, 'topfit-straight'] = 0
        
        data.at[i, 'botfit-fitted'] = 0
        data.at[i, 'botfit-loose'] = 0
        data.at[i, 'botfit-oversized'] = 0
        data.at[i, 'botfit-straight'] = 0
        
        data.at[i, 'category-petite'] = 0
        data.at[i, 'category-regular'] = 0
        data.at[i, 'category-plus'] = 0
        data.at[i, 'category-tall'] = 0
        
        data.at[i, 'set-Mostly dresses and skirts'] = 0
        data.at[i, 'set-Mostly pants and jeans'] = 0
        
        data.at[i, 'looks-bohemian & earthy'] = 0
        data.at[i, 'looks-bold & daring'] = 0
        data.at[i, 'looks-chic & on trend'] = 0
        data.at[i, 'looks-classic & preppy'] = 0
        data.at[i, 'looks-cool & edgy'] = 0
        data.at[i, 'looks-glam & feminine'] = 0
        data.at[i, 'looks-minimal & understated'] = 0
        data.at[i, 'looks-sharp & polished'] = 0
        data.at[i, 'looks-urban & effortless'] = 0
        data.at[i, 'looks-artistic & eclectic'] = 0

        height = data.at[i, 'height']
        weight = data.at[i, 'weight']
        category = getCategory(int(float(height)), int(float(weight)))
        
        data.at[i,'category-' + category] = 1
        
        looks_represent = data.looks_represent[i]
        if(type(looks_represent) != int):
            for look in looks_represent.split(','):
                string = look.strip().lower()
                data.at[i, 'looks-'+string] = 1
    
    
        colours = data.colour_palettes[i]    
        if( type(colours) != int ):
            for col in colours.split(','):
                string = col.strip().lower()
                if(string == 'i like all palettes'):
                    data.at[i, 'palette-bright'] = 1
                    data.at[i, 'palette-cool'] = 1
                    data.at[i, 'palette-warm'] = 1
                    data.at[i, 'palette-neutral'] = 1
                    data.at[i, 'palette-understated'] = 1
                else:
                    data.at[i, 'palette-'+string] = 1
                
        topfits = data.top_fit[i]
        if( type(topfits) != int):
            for top in topfits.split(','):
                string = top.strip().lower()
                data.at[i, 'topfit-'+string] = 1
                
        botfits = data.bot_fit[i]
        if( type(botfits) != int):
            for bot in botfits.split(','):
                string = bot.strip().lower()
                data.at[i, 'botfit-'+string] = 1
                
        set = data.style_set[i]
        if(set == 'Mix of both'):
            data.at[i, 'set-Mostly dresses and skirts'] = 1
            data.at[i, 'set-Mostly pants and jeans'] = 1
        else:  
            data.at[i, 'set-'+set] = 1
   
    data.drop(['height', 'weight', 'looks_represent', 'style_set', 'colour_palettes', 'top_fit', 'bot_fit'], 1, inplace=True)
    data = data.reindex(sorted(data.columns), axis=1)
    data=data[[
        'hijab',
        
        'category-petite',
        'category-plus',
        'category-regular',
        'category-tall',
        
        'looks-artistic & eclectic',
        'looks-bohemian & earthy',
        'looks-bold & daring',
        'looks-chic & on trend',
        'looks-classic & preppy',
        'looks-cool & edgy',
        'looks-glam & feminine',
        'looks-minimal & understated',
        'looks-sharp & polished',
        'looks-urban & effortless',
        
        'topfit-fitted',
        'topfit-loose',
        'topfit-oversized',
        'topfit-straight',
        
        'botfit-fitted',
        'botfit-loose',
        'botfit-oversized',
        'botfit-straight',
            
        'palette-bright',
        'palette-cool',
        'palette-neutral',
        'palette-understated',
        'palette-warm',
        
        'set-Mostly dresses and skirts',
        'set-Mostly pants and jeans',

        'cover-top',
        'cover-bottom'
    ]]
    return data

def getCluster(data, isHijab):
    if isHijab:
        data_hijab = data[data.hijab == True].copy(deep=True)
        data_hijab.drop(['hijab'], 1 ,inplace=True)
       
        X = np.array(data_hijab.astype(float))

        scaler = pk.load(open('./scaler/latest_scaler_hijab2.pkl', 'rb'), encoding='latin1')

        X = scaler.transform(X)

        X[:,0:4] = X[:,0:4] * 1.2       ### CATEGORY
        X[:,4:14] = X[:,4:14] * 0.9     ### LOOKS
        X[:,14:18] = X[:,14:18] * 0.0  ### TOP FIT
        X[:,18:22] = X[:,18:22] * 0.0  ### BOT FIT
        X[:,22:27] = X[:,22:27] * 0.0  ### COLOUR PALETTE
        X[:,27:29] = X[:,27:29] * 0.9   ### SET
        X[:,29:31] = X[:,29:31] * 0.0   ### COVER

        clf = pk.load(open('./classifier/latest_hijab_classifier2.pkl','rb'), encoding='latin1')
        new_labels = clf.predict(X)
        return new_labels[0]
    else:
        data_non_hijab = data[data.hijab == False].copy(deep=True)
        data_non_hijab.drop(['hijab'], 1 ,inplace=True)

        X = np.array(data_non_hijab.astype(float))
        scaler = pk.load(open('./scaler/latest_scaler_non_hijab2.pkl', 'rb'), encoding='latin1')
        X = scaler.transform(X)

        X[:,0:4] = X[:,0:4] * 1.2       ### CATEGORY
        X[:,4:14] = X[:,4:14] * 0.9     ### LOOKS
        X[:,14:18] = X[:,14:18] * 0.0  ### TOP FIT
        X[:,18:22] = X[:,18:22] * 0.0  ### BOT FIT
        X[:,22:27] = X[:,22:27] * 0.0  ### COLOUR PALETTE
        X[:,27:29] = X[:,27:29] * 0.8   ### SET
        X[:,29:31] = X[:,29:31] * 0.8   ### COVER

        clf = pk.load(open('./classifier/latest_non_hijab_classifier2.pkl','rb'), encoding='latin1')
        new_labels = clf.predict(X)
        return new_labels[0]

def calculateCluster(data):
    # df = preprocessing(createDataFrame(getFeatures(data)))
    # cluster = getCluster(df, df.at[0, 'hijab'])
    # return cluster, df.at[0, 'hijab']
    try:
        df = preprocessing(createDataFrame(getFeatures(data)))
        cluster = getCluster(df, df.at[0, 'hijab'])
        return cluster, df.at[0, 'hijab']
    except:
        return 404, 'error'

# =================================================================================================
# ROUTES
# =================================================================================================

@cluster.route('/test', methods=['GET'])
def test():
    return '/cluster is working fine 😘'

@cluster.route('/user/<userId>', methods=['GET'])
def index(userId):
    id = userId

    if re.search('[a-zA-Z]', id):
        data = {
           'error': 'no user found or user data is incomplete'
        }
        return jsonify(data), 404

    response = requests.get( url + 'third-party/data-client/' + id, headers=headers)
    print("hello")
    print(response.json())
    cluster, isHijab = calculateCluster(response.json()['spq'])

    if cluster == 404:
        data = {
           'error': 'no user found or user data is incomplete'
        }
        return jsonify(data), 404
    else:
        data = {
            'cluster_type': 'hijab' if isHijab else 'non_hijab',
            'cluster_no': int(cluster)
        }
        return jsonify(data), 200