
# from flask import Flask, Blueprint,  jsonify, request

# import numpy as np # linear algebra
# import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
# import os # accessing directory structure
# import cv2
# import io
# from google.cloud import vision
# import tensorflow as tf
# import keras
# from keras import Model
# from keras.applications.resnet50 import ResNet50
# from keras.preprocessing import image
# from keras.applications.resnet50 import preprocess_input, decode_predictions
# from keras.layers import GlobalMaxPooling2D
# from sklearn.metrics.pairwise import pairwise_distances
# from PIL import Image, ImageDraw
# import uuid
# import requests # to get image from the web
# import shutil # to save it locally

# os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = r'key.json'

# predictor = Blueprint('predictor', __name__, url_prefix='/predictor')

# # =================================================================================================
# # FUNCTION
# # =================================================================================================

# def img_path(img):
#     return img

# def load_image(img, resized_fac = 0.5, resize=True):
#     img     = cv2.imread(img_path(img))
#     w, h, _ = img.shape
#     if(resize):   
#         resized = cv2.resize(img, (int(h*resized_fac), int(w*resized_fac)), interpolation = cv2.INTER_AREA)
#         return resized
#     else:
#         return img

# def load_model():
#     # Input Shape
#     img_width, img_height, _ = 224, 224, 3 #load_image(df.iloc[0].image).shape

#     # load model
#     # model = keras.models.load_model('./config/models/test200')

#     # Pre-Trained Model
#     base_model = ResNet50(weights='imagenet', 
#                         include_top=False, 
#                         input_shape = (img_width, img_height, 3))
#     base_model.trainable = False

#     # Add Layer Embedding
#     model = keras.Sequential([
#         base_model,
#         GlobalMaxPooling2D()
#     ])

#     return model

# def localize_objects(path):
#     results = []
    
#     """Localize objects in the local image.

#     Args:
#     path: The path to the local file.
    
#     """
    
#     timg = load_image(path, 1, False)

#     dimensions = timg.shape

#     # height, width, number of channels in image
#     height = timg.shape[0]
#     width = timg.shape[1]
    
#     from google.cloud import vision
#     client = vision.ImageAnnotatorClient()

#     with open(path, 'rb') as image_file:
#         content = image_file.read()
#     image = vision.Image(content=content)

#     objects = client.object_localization(
#         image=image).localized_object_annotations
    
#     results = []
#     object_name = []
#     for object_ in objects:
#         name = object_.name
#         score = object_.score
#         if((name != 'Person') and (name not in object_name) and (score > 0.60)):
#             object_name.append(name)
#             data = {
#                 'name' : name,
#                 'confidence': score,
#                 'vertexes': []
#             }
#             verx = []

#             for vertex in object_.bounding_poly.normalized_vertices:
#                 data['vertexes'].append([int(vertex.x * width), int(vertex.y * height)])


#             results.append(data)
#     return results

# def get_embedding(model, img_name):
#     # Reshape
#     img = image.load_img(img_path(img_name), target_size=(224, 224))
#     # img to Array
#     x   = image.img_to_array(img)
#     # Expand Dim (1, w, h)
#     x   = np.expand_dims(x, axis=0)
#     # Pre process Input
#     x   = preprocess_input(x)
#     return model.predict(x).reshape(-1)

# def download_image(image_url, filename):    
#     # Open the url image, set stream to True, this will return the stream content.
#     r = requests.get(image_url, stream = True)

#     # Check if the image was retrieved successfully
#     if r.status_code == 200:
#         # Set decode_content value to True, otherwise the downloaded image file's size will be zero.
#         r.raw.decode_content = True

#         # Open a local file with wb ( write binary ) permission.
#         with open(filename,'wb') as f:
#             shutil.copyfileobj(r.raw, f)
#     else:
#        raise Exception("cant download image, please check the given link")

# def get_recommender(idx, df, df_embs, cosine_sim, indices, top_n = 5):
#     sim_idx    = indices[idx]
#     sim_scores = list(enumerate(cosine_sim[sim_idx]))
#     sim_scores = sorted(sim_scores, key=lambda x: x[1], reverse=True)
#     sim_scores = sim_scores[1:top_n+1]
#     idx_rec    = [i[0] for i in sim_scores]
#     idx_sim    = [i[1] for i in sim_scores]
    
#     return indices.iloc[idx_rec].index, idx_sim

# def get_variantIds(uploaded, df_original_count, df, df_embs, cosine_sim, indices):
#     new_items = []
#     for i in range(len(uploaded)):
#         new_items.append(df_original_count + i)
        
#     variant_ids = []

#     print(new_items)

#     for i in range(len(new_items)):
#         idx_rec, idx_sim = get_recommender(new_items[i], df, df_embs, cosine_sim, indices, top_n = 6)
#         filter_arr = []

#         for element in idx_rec:
#             if element < df_original_count:
#                 filter_arr.append(True)
#             else:
#                 filter_arr.append(False)
#         idx_rec = idx_rec[filter_arr]
        
#         for j in range(len(idx_rec)):
#             variant_ids.append(df.loc[idx_rec[j]]['variant_id'])

#     variant_ids = set(variant_ids)
#     variant_ids = list(variant_ids)
#     return variant_ids

# def predict_sets(image):
#     model = load_model()

#     prefix = 'https://assets.helloyuna.io/'
#     query = '?height=250&width=250&crop=fit'
#     dlink = image
#     extension = dlink[-4:]
#     imageFile = 'dataset/uploaded/upload-' + str(uuid.uuid4()) + extension

#     download_image(prefix + dlink + query, imageFile)
#     uploaded = localize_objects(imageFile)

#     croppedImage = []
#     for i in range(len(uploaded)):
#         v = uploaded[i]['vertexes']
        
#         im = Image.open(imageFile)
#         rgb_im = im.convert('RGB')

#         im2 = rgb_im.crop([v[0][0], v[0][1],
#                         v[2][0] - 1, v[2][1] - 1])
#         croppedImage.append('dataset/cropped_images/output-crop-'+str(i)+'.jpg')
#         im2.save('dataset/cropped_images/output-crop-'+str(i)+'.jpg', 'JPEG')
    
#     df_embs = pd.read_pickle('config/embs/testAll.pkl')

#     df = pd.read_excel('dataset/data/processed/data_variant_processed_280121.xlsx', engine='openpyxl')
#     df.drop(['Unnamed: 0'], 1 , inplace=True)

#     df_original_count = len(df)

#     df_addition = pd.DataFrame(columns=['image', 'variant_id', 'product'])
#     img_count = len(uploaded)
#     for i in range(img_count):
#         df_addition.at[len(df_embs)+i, 'image'] = croppedImage[i]
#         df_addition.at[len(df_embs)+i, 'variant_id'] = len(df_embs)+i

#     df = df.append(df_addition)

#     # Parallel apply
#     df_sample      = df_addition
#     map_embeddings = df_sample['image'].apply(lambda img: get_embedding(model, img))
#     df_embs_addition = map_embeddings.apply(pd.Series)

#     df_embs = df_embs.append(df_embs_addition)

#     # Calcule DIstance Matriz
#     cosine_sim = 1-pairwise_distances(df_embs, metric='cosine')
#     indices = pd.Series(range(len(df)), index=df.index)
#     os.remove(imageFile)
#     results = get_variantIds(uploaded, df_original_count, df, df_embs, cosine_sim, indices)
#     del df
#     del df_embs
#     del model
#     return results



# # =================================================================================================
# # ROUTES
# # =================================================================================================

# @predictor.route('/test', methods=['GET'])
# def test():
#     body = request.get_json()
#     return body['image']

# @predictor.route('/predict', methods=['GET'])
# def index():
#     body = request.get_json()
#     return jsonify(predict_sets(body['image'])), 200

