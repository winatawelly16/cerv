from flask import Flask
from flask_cors import CORS

from routes.cluster import cluster
# from routes.predictor import predictor


# EB looks for an 'application' callable by default.
application = Flask(__name__)
CORS(application)

application.register_blueprint(cluster)
# application.register_blueprint(predictor)


@application.route('/')
def welcome():
    return 'haibro prod😘'
   
# run the app.
if __name__ == "__main__":
    # Setting debug to True enables debug output. This line should be
    # removed before deploying a production app.
    application.debug = True
    application.run()